package main

import (
	"fmt"
	"time"
)

func main() {
	//runtime.GOMAXPROCS(1)

	fmt.Println("start.")
	go func () {
		time.Sleep(100*time.Millisecond)
		fmt.Println("hello")
	}()
	//time.Sleep(200*time.Millisecond)
	fmt.Println("end.")
}