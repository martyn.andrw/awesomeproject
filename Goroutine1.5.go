package main

import (
	"fmt"
	"runtime"
	"time"
)

func wait() {
	for i := 0; i < 3; i++ {
		time.Sleep(100 * time.Millisecond)
	}
}

func main() {
	fmt.Println("Число ядер/процессоров: ", runtime.NumCPU())
	fmt.Println("До запуска горутин: ", runtime.NumGoroutine())

	go wait()
	go wait()
	fmt.Println("После запуска горутин: ", runtime.NumGoroutine())
	time.Sleep(500 * time.Millisecond)
}