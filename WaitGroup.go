package main

import (
	"fmt"
	"sync"
)

func main() {
	text := "he"

	wg := sync.WaitGroup{}
	wg.Add(5)
	for i := 0; i < 5; i++ {
		go func (){
			fmt.Print(text)
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println()
}