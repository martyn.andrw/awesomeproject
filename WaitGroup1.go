package main

import (
	"fmt"
	"sync"
)

func main() {
	text := "he"

	wg := sync.WaitGroup{}
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func (){
			fmt.Print(text)
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println()
}