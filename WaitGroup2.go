package main

import (
	"fmt"
	"sync"
)

func main() {
	intVar := 0

	wg := sync.WaitGroup{}
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func (){
			intVar++
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println(intVar)
}