package main

import (
	"fmt"
)

func main() {
	variable := 0

	for i := 0; i < 100; i++ {
		go func (){
			variable++
		}()
	}
	fmt.Println(variable)
}