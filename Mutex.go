package main

import (
	"fmt"
	"sync"
)

func main() {
	intVar := 0

	wg := sync.WaitGroup{}
	mu := sync.Mutex{}

	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func (){
			mu.Lock()
			defer mu.Unlock()
			defer wg.Done()
			intVar++
		}()
	}
	wg.Wait()
	fmt.Println(intVar)
}