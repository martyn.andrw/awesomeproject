package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const count int = 5

var mtx sync.Mutex
var wg sync.WaitGroup
var fed = 0

type Forks struct {
	forks [count]int
}

type Gofers struct {
	gofers [count]int
}

func (f *Forks) pickLeftFork(goferId int){
	f.forks[goferId] = 1
}

func (f *Forks) pickRightFork(goferId int){
	f.forks[(goferId- 1 + count) % count] = 1
}

func (f *Forks) putLeftFork(goferId int){
	f.forks[goferId] = 0
}

func (f *Forks) putRightFork(goferId int){
	f.forks[(goferId- 1 + count) % count] = 0
}

func wantsToEat(goferId int, gofers *Gofers, forks *Forks) {
	fmt.Printf("Gofer <%d> встал в очередь на трапезу\n", goferId)
	isStartedFeeding := false

	mtx.Lock() // лучше всего использовать остановку горутины пока вилки заняты или семафоры, но я пока не разобрался ни с тем, ни с другим
	if forks.forks[goferId] == 0 && forks.forks[(goferId- 1 + count) % count] == 0 {
		forks.pickLeftFork(goferId)
		forks.pickRightFork(goferId)
		fmt.Printf("Gofer <%d> кушает\n", goferId)
		gofers.gofers[goferId] = 1 // the gofer will be fed by the goroutine
		isStartedFeeding = true // the gofer took 2 forks
	}
	mtx.Unlock()

	millisecond := rand.Int63n(3000) // time to feed our gofer
	time.Sleep(time.Duration(millisecond) * time.Millisecond)
	fmt.Println(forks)

	if isStartedFeeding {
		mtx.Lock()
		forks.putLeftFork(goferId)
		forks.putRightFork(goferId)
		fed++
		fmt.Printf("Gofer <%d> думает о горутинах\n", goferId)
		mtx.Unlock()
	}

	wg.Done()
}

func newRoundGofers(gofers *Gofers){
	for i := 0; i < count; i++{
		gofers.gofers[i] = 0
		fed = 0
	}
}

func waiter(gofers *Gofers, forks *Forks){
	wg = sync.WaitGroup{}
	for {
		if fed == 5 {
			break
		}
		for key, value := range gofers.gofers {
			if value == 0 {
				wg.Add(1)
				go wantsToEat(key, gofers, forks)
			}
		}
		time.Sleep(1 * time.Second)
	}
	wg.Wait()
}

func main() {
	var gofers Gofers
	var forks Forks

	howManyTimesDoYouWantToFeedGofers := 1
	for i := 0; i < howManyTimesDoYouWantToFeedGofers; i++ {
		newRoundGofers(&gofers)
		waiter(&gofers, &forks)
	}
	fmt.Println("Все гоферы сыты и довольны :)")
}
